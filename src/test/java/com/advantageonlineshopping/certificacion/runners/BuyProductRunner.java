package com.advantageonlineshopping.certificacion.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/BuyProduct.feature",
        glue = {"com.advantageonlineshopping.certificacion.stepdefinitions"},
        snippets = SnippetType.CAMELCASE)
public class BuyProductRunner {
}
