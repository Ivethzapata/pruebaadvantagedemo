package com.advantageonlineshopping.certificacion.questions;

import com.advantageonlineshopping.certificacion.interactions.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static com.advantageonlineshopping.certificacion.userinterface.LoginPage.USER_NAME;

public class TheLoginIsSuccessful implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        actor.attemptsTo(Wait.aMoment(2));
        return  Text.of(USER_NAME).viewedBy(actor).asString();
    }
    public static TheLoginIsSuccessful inPage() {
        return new TheLoginIsSuccessful();
    }
}
